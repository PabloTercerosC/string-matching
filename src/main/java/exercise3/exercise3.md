## Exercise 3

Given a text T, a pattern P, and a number x (𝑥 ≤ |𝑇 |) followed by x numbers. For each
T[1,...,x] find the longest suffix of T[1,...,x] that matches the longest prefix of P

### Algorithm

We used the **Finite Automaton** because its transition function (more specific the next state function) has the key to solve this problem.

### Changes

- **General:** The main changes came in the next state method, where instead of comparing Q x Sigma (alphabet), we compared Q x T, where each q were compared by each t only once, so we had an array solution instead of a matrix one.
```java
    static int getNextState(char[] pat, int M,
        int state, char[] txt) {

        int ns, i;

        if(state < M && txt[state] == pat[state])
        return 1;

        for (ns = state; ns > 0; ns--) {
        if (pat[ns - 1] == txt[state]) {
        for (i = 0; i < ns - 1; i++)
        if (pat[i] != txt[state - ns + 1 + i])
        break;
        if (i == ns - 1)
        return ns;
        }
        }

        return 0;
        }
```

- **Time Complexity O:**

We have a O(n) time complexity, as we only iterate over N O(N).