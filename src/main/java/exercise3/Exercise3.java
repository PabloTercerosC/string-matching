package exercise3;

import java.util.Arrays;

public class Exercise3 {

    static int getNextState(char[] pat, int M,
                            int state, char[] txt) {

        int ns, i;

        if(state < M && txt[state] == pat[state])
            return 1;

        for (ns = state; ns > 0; ns--) {
            if (pat[ns - 1] == txt[state]) {
                for (i = 0; i < ns - 1; i++)
                    if (pat[i] != txt[state - ns + 1 + i])
                        break;
                if (i == ns - 1)
                    return ns;
            }
        }

        return 0;
    }

    static void computeTF(char[] pat, int M, char[] txt, int[] TF) {
        int state;

        for (state = 0; state <= M; ++state) {
            TF[state] = getNextState(pat, M, state, txt);
        }
        System.out.println(Arrays.toString(TF));
    }

    static void search(char[] pat, char[] txt) {
        int M = pat.length;
        int N = txt.length;

        int[] TF = new int[N];

        computeTF(pat, M, txt, TF);
    }

    public static void main(String[] args) {
        char[] pat = "abba".toCharArray();
        char[] txt = "bab".toCharArray();
        search(txt, pat);
    }
}