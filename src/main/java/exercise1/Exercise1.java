package exercise1;

public class Exercise1 {

    public final static int D = 10;

    static void search(String pattern, String txt, int q) {
        int m = pattern.length();
        int n = txt.length();
        int i, j;
        int p = 0;
        int t = 0;
        int h = 1;

        for (i = 0; i < m - 1; i++)
            h = (h * D) % q;


        for (i = 0; i < m; i++) {
            p = (D * p + pattern.charAt(i)) % q;
            t = (D * t + txt.charAt(i)) % q;
        }

        for (i = 0; i <= n - m; i++) {
            if (p == t) {
                for (j = 0; j < m; j++) {
                    if (txt.charAt(i + j) != pattern.charAt(j)) {
                        System.out.println("At position: " + (i + 1));
                        break;
                    }
                }
            }

            if (i < n - m) {
                t = (D * (t - txt.charAt(i) * h) + txt.charAt(i + m)) % q;
                if (t < 0)
                    t = (t + q);
            }
        }
    }

    public static void main(String[] args) {
        //ABC and CDD have the same hash value
        String txt = "ABCCDDAABC";
        String pattern = "CDD";
        int q = 13;
        System.out.println("Same hash value but does not match P:");
        search(pattern, txt, q);
    }
}