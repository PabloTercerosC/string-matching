## Exercise 1

Given a T and P, find all the T[s,..,s+m] that have the same hash value as the pattern P
but do not match P.

### Algorithm

We used the **Rabin-Karp** algorithm, as hinted by the exercise itself: "...find all the T[s,..,s+m] that have the same hash value..." the algorithm calculates exactly that, the hash values of two strings T and P. So we used that logic to solve the problem.

### Changes

- **General:** Our main changes were made here, inside this loop... 
```java
      //Original cycle
      for (i = 0; i <= n - m; i++) {
        if (p == t) {
            for (j = 0; j < m; j++) {
                if (txt.charAt(i + j) != pattern.charAt(j))
                 break;
            }

        if (j == m)
            System.out.println("Pattern is found at position: " + (i + 1));
        }
```
Here if the if p and t are equal (hash codes are equal) we enter a second condition where if they different even by a single character we discard the occurrence. But in the new implementation...
```java
        //Modified cycle
        for (i = 0; i <= n - m; i++) {
            if (p == t) {
                for (j = 0; j < m; j++) {
                    if (txt.charAt(i + j) != pattern.charAt(j)) {
                        System.out.println("At position: " + (i + 1));
                        break;
                    }
                }
```
We change it so we still check the hash codes, but in the second condition we pick the position if they are not equal.

- **Time Complexity O:**

There was no change in the time complexity of the solution.