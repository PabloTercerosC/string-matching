package exercise2;

class Exercise2 {
    int KMPSearch(String pat, String txt) {
        int M = pat.length();
        int N = txt.length();
        int occurrences = 0;

        int lps[] = new int[M];
        int j = 0;

        computeLPSArray(pat, M, lps);

        int i = 0;
        while ((N - i) >= (M - j)) {
            if (pat.charAt(j) == txt.charAt(i)) {
                j++;
                i++;
            }
            if (j == M) {
                occurrences++;
                j = lps[j - 1];
            }

            else if (i < N
                    && pat.charAt(j) != txt.charAt(i)) {
                if (j != 0)
                    j = lps[j - 1];
                else
                    i = i + 1;
            }
        }

        return occurrences;
    }

    void computeLPSArray(String pat, int M, int lps[]) {
        int len = 0;
        int i = 1;
        lps[0] = 0;

        while (i < M) {
            if (pat.charAt(i) == pat.charAt(len)) {
                len++;
                lps[i] = len;
                i++;
            } else
            {
                if (len != 0) {
                    len = lps[len - 1];

                } else
                {
                    lps[i] = len;
                    i++;
                }
            }
        }
    }

    public static void main(String args[]) {
        String txt = "abababc";
        String pat = "abababc";
        String prefix = "";
        int occurrences = 0;

        for (int i = 1; i < pat.length(); i++) {
            String p = pat.substring(0, i);
            int o = new Exercise2().KMPSearch(p, txt);

            if (o >= occurrences && p.length() > prefix.length()) {
                occurrences = o;
                prefix = p;
            }
        }
        System.out.println(prefix + "-" + occurrences);
    }
}