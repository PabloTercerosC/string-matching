## Exercise 2

Given a string S, print the prefix that matches the most in S. If there are multiple answers,
print the longest prefix. Print the word and how many times it matches. (E.g. S = abababc,
output: ab - 3)

### Algorithm

We used the **Knuth-Morris-Pratt** because it helps us find very efficiently the occurrences of prefixes in the string, which comes very handy because we are visiting every prefix in the string. 

### Changes

- **General:** We mainly changed two things. Firstly, we added an occurrences variables to save the number of ocurrences...
```java
    int KMPSearch(String pat, String txt) {
        int M = pat.length();
        int N = txt.length();
        int occurrences = 0;
```
... and instead of printing the position of the occurrences we saved them in the variable:
```java
        int i = 0;
        while ((N - i) >= (M - j)) {
            if (pat.charAt(j) == txt.charAt(i)) {
                j++;
                i++;
            }
        if (j == M) {
            occurrences++;
            j = lps[j - 1];
        }
```
And secondly, we changed the main code so we would check every prefix instead of a single T string:
```java
    public static void main(String args[]) {
        String txt = "abababc";
        String pat = "abababc";
        String prefix = "";
        int occurrences = 0;

        for (int i = 1; i < pat.length(); i++) {
            String p = pat.substring(0, i);
            int o = new Exercise2().KMPSearch(p, txt);

            if (o >= occurrences && p.length() > prefix.length()) {
                occurrences = o;
                prefix = p;
            }
        }
        System.out.println(prefix + "-" + occurrences);
    }
```

- **Time Complexity O:**

So we have the same Time Complexity overall, plus we iterate for every prefix of the string, so we should multiply it by S size.